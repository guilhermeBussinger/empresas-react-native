Algumas considerações sobre este projeto:

Como este projeto foi minha primeira experiencia com o React Native, aprendi muito sobre essa framework e como ela funciona, conseguindo me adaptar rapidamente pelo conhecimento em javascript.
A única endpoint que faltou foi filtrar por nome e tipo. Alguns outros detalhes como o estilo, transição de telas com loading e disposição de alguns scripts de requisição poderiam ser melhoradas.
Desde já agradeço a oportunidade de participar do processo seletivo e a oportunidade de aprender sobre esta framework que gostei muito de trabalhar!