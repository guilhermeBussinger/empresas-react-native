import React from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';

//importando styles
import styleLogin from '../../styles/Login/login';

//importando funcoes de negocio
import { logar } from '../../negocio/Login';

export default class Login extends React.Component {
    static navigationOptions = {
        headerStyle: styleLogin.headerLogin
    };

    constructor(){
        super();
  
        this.state = {
            Email: '',
            Senha: ''
        }  
    }

  render() {
    const {navigate} = this.props.navigation;
    return (
        <View style={styleLogin.fundo}>
            <View style={styleLogin.areaLogin}>
                <Text style={styleLogin.tituloLogin}>Fazer Login</Text>

                <TextInput  style= {styleLogin.inputLogin}
                placeholder="Email"
                onChangeText={(email) => this.setState({Email: email})}
                />

                <TextInput style= {styleLogin.inputLogin}
                placeholder="Senha"
                onChangeText={(senha) => this.setState({Senha: senha})}
                secureTextEntry={true}
                />

                <TouchableOpacity 
                    style={styleLogin.botaoLogin}
                    onPress={() => {
                        let email = this.state.Email;
                        let senha = this.state.Senha;
                        logar({
                            Email: email,
                            Senha: senha,
                            CallbackSucesso: () => {
                                navigate('Inicio');
                            }
                        });
                    }}>
                    <Text> Login </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
  }
}