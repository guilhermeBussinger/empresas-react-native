import React from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';

//importando styles
import styleGeral from '../../styles/geral';
import styleDetalhes from '../../styles/DetalhesEmpresa/detalhesEmpresa';

//importando funcoes de negocio
import { recuperarEmpresaPorId } from '../../negocio/Empresas'

export default class Login extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
          title: navigation.getParam('nomeEmpresa', 'Erro')
        };
    };

    constructor(){
        super();
  
        this.state = {
            loading: false
        }  
    }

    static empresa;

    componentWillMount(){
        this.setState({ loading: true});
        const { navigation } = this.props;
        const itemId = navigation.getParam('itemId', '0');
        AsyncStorage.getItem('headers', (error,value) => {
            if (!error) {
                if (value !== null){
                    let headers = JSON.parse(value);

                    recuperarEmpresaPorId({
                        Id: itemId,
                        Headers: {
                            acessToken: headers.acessToken,
                            client: headers.client,
                            uid: headers.uid
                        },
                        CallbackSucesso: (item) => {
                            empresa = item;
                            this.setState({ loading: false});
                        }
                    });
                } 
            }else{
                alert("Erro ao recuperar dados!");
            }
        });
    }

    render() {
        if(this.state.loading) return null;
        else {
            return (
                <View style={styleGeral.fundo}>
                        <View style = { styleDetalhes.cabecalho}>
                            <Image style = { styleDetalhes.avatar } source = {{ uri: empresa.photo === null ? 'https://conexaofreelancer.com.br/wp-content/uploads/2018/06/Pessoa.png' : 'http://empresas.ioasys.com.br/' + empresa.photo }} ></Image>
                                <View style = { styleDetalhes.containerNome }>
                                    <Text style={ styleDetalhes.nomeEmpresa }>
                                        { empresa.enterprise_name }
                                    </Text>
                                    <Text style={ styleDetalhes.nomeTipo }>
                                        { empresa.enterprise_type.enterprise_type_name }
                                    </Text>
                                    <Text>
                                        { empresa.city }, { empresa.country }
                                    </Text>
                                </View>
                        </View>
                        <Text style = {styleDetalhes.containerDescricao}>
                            { empresa.description }
                        </Text>
                </View>
            );
        }
    }
}