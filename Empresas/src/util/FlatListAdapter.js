import React from 'react';
import { AsyncStorage, FlatList, View, Text, Image, TouchableOpacity } from 'react-native';

import styleGeral from '../styles/geral';
import styleInicio from '../styles/Inicio/inicio';

class FlatListAdapter extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            data: []
        };
    }

    static navigationOptions = {
        headerLeft: null,
        headerStyle: styleInicio.headerInicio
    };

    componentDidMount(){
        AsyncStorage.getItem('headers', (error,value) => {
            if (!error) {
                if (value !== null){
                    let headers = JSON.parse(value);

                    fetch('http://empresas.ioasys.com.br/api/v1/enterprises', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'access-token': headers.acessToken,
                        'client': headers.client,
                        'uid': headers.uid
                    },
                    })
                    .then(resposta => resposta.json()) 
                    .then(respostaJson => {
                        this.setState({
                            data: respostaJson.enterprises
                        });
                    })
                    .catch((error) => {
                        console.error(error);
                        alert("Erro na conexão!");
                    });
                } 
            }else{
                alert("Erro recuperando dados!");
            }
        });
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity  style = { styleInicio.linhaLista } onPress = {() => this.onPressItem(item.id, item.enterprise_name)}>
            <Image style = { styleInicio.avatar }
                    source = {{ uri: item.photo === null ? 'https://conexaofreelancer.com.br/wp-content/uploads/2018/06/Pessoa.png' : 'http://empresas.ioasys.com.br/' + item.photo}} ></Image>
                <View style = {styleInicio.containerTitulo}>
                    <Text style={styleInicio.tituloItem}> { item.enterprise_name }</Text>
                    <Text style={styleInicio.subtituloItem}> { item.enterprise_type.enterprise_type_name }</Text>
                </View>
            </TouchableOpacity >
        )
    }

    keyExtractor = (item, index) => toString(item.id);

    onPressItem(id, nome){
        this.props.navigation.navigate('DetalhesEmpresa', {
            itemId: id,
            nomeEmpresa: nome
        });
    };

    render(){
        return (
            <View style = {styleGeral.fundo}>
                <FlatList
                    data={this.state.data}
                    renderItem={ this.renderItem }
                    keyExtractor={ this.keyExtractor }
                />
            </View>
        );
    }
}

export default FlatListAdapter;