import { createStackNavigator } from 'react-navigation';

//importando telas
import Login from '../components/Login/login';
import Inicio from '../components/Inicio/inicio';
import DetalhesEmpresa from '../components/DetalhesEmpresa/detalhesEmpresa';

const MainNavigator = createStackNavigator({
    Login: { screen: Login },
    Inicio: { screen: Inicio },
    DetalhesEmpresa: { screen: DetalhesEmpresa }
  });

export default MainNavigator;