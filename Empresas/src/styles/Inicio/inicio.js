import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    linhaLista: { 
        flex: 1, 
        flexDirection: 'row',
        marginTop: 10,
        paddingLeft: 5,
        paddingBottom: 8,
        backgroundColor: "#e7eff6",
        borderBottomWidth: 1,
        borderBottomColor: "#CDCDCD"
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 50
    },
    containerTitulo: {
        flex: 1, 
        flexDirection: 'column',
    },
    tituloItem: {
        fontSize: 20,
        marginTop: 15,
        marginLeft: 10
    },
    subtituloItem: {
        marginLeft: 10
    },
    headerInicio: {
        height: 0
    }
});