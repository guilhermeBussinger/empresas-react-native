import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    areaLogin: {
        flex: 1, 
        alignItems: 'center',
        marginTop: "60%"
    },
    tituloLogin: {
        fontSize: 24
    },
    inputLogin: {
        backgroundColor: "white",
        width: "60%",
        marginTop: 5,
        borderRadius: 10
    },
    botaoLogin: {
        marginTop: 10,
        height: 30,
        width: "30%",
        backgroundColor: "#b3cde0",
        alignItems: 'center', 
        justifyContent: 'center',
        borderRadius: 10
    },
    headerLogin: {
        height: 0
    },
    fundo: {
        backgroundColor: "#63ace5",
        flex: 1
    }
});