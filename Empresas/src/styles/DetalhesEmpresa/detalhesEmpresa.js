import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    cabecalho: {
        width: "100%",
        flexDirection: "row",
        height: 130
    },
    avatar: {
        height: 110,
        width: 110,
        borderRadius: 50,
        marginTop: 10,
        marginLeft: 10
    },
    containerNome: {
        paddingTop: 15,
        paddingLeft: 15
    },
    nomeEmpresa: {
        fontSize: 30
    },
    nomeTipo: {
        fontSize: 20
    },
    containerDescricao: {
        padding: 15
    }
});