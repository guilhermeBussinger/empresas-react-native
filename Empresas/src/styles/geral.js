import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    fundo: {
        backgroundColor: "#e7eff6",
        flex: 1
    }
});