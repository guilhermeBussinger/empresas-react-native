import { AsyncStorage } from 'react-native';

export function logar(parametros) {
    fetch('http://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        email: parametros.Email,
        password: parametros.Senha,
    }),
    }).then(function(response) {

        //salva os headers necessarios para as outras requisicoes
        let headers = {
            acessToken: response.headers.get('access-token'),
            client: response.headers.get('client'),
            uid: response.headers.get('uid')
        }

        AsyncStorage.setItem('headers', JSON.stringify(headers));

        response.json().then(function(responseJson) {
            
            if(responseJson.success === false){
                alert(responseJson.errors[0]);
            };

            if(parametros.CallbackSucesso !== undefined){
                parametros.CallbackSucesso();
            }

        });
    })
    .catch((error) => {
        console.error(error);
        alert("Erro na conexão!");
    });
}