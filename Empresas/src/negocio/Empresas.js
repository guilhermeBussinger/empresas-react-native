export function recuperarEmpresaPorId(parametros){
    const headersUsuario = parametros.Headers;
    
    fetch('http://empresas.ioasys.com.br/api/v1/enterprises/' + parametros.Id, {
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'access-token': headersUsuario.acessToken,
        'client': headersUsuario.client,
        'uid': headersUsuario.uid
    },
    })
    .then(resposta => resposta.json()) 
    .then(respostaJson => {

        if(parametros.CallbackSucesso !== undefined){
            parametros.CallbackSucesso(respostaJson.enterprise);
        }
    })
    .catch((error) => {
        console.error(error);
        alert("Erro na conexão!");
    });
}